#include <Arduino.h>
#include <SoftwareSerial.h>

#define INPIN   10
#define OUTPIN  11

void setup();
void loop();

SoftwareSerial serial = SoftwareSerial(INPIN, OUTPIN);

void getsubstr(const char *src, char *dest, int offset, int n)
{
  for (int i = 0; i < n; i++)
    dest[i] = src[offset + i];

  dest[n] = 0;
}

void setup()
{
  pinMode(INPIN, INPUT);
  pinMode(OUTPIN, OUTPUT);

  Serial.begin(9600);
  serial.begin(9600);

  delay(2000);
  serial.print('U');

  while (serial.read() != ':');
  delay(10);

  Serial.flush();
  serial.flush();
}

void loop()
{
  serial.print('R');

  char  data[16];
  char  cleardata[8];
  int   offset = 0;

  while (1)
  {
    if (serial.available() > 0)
    {
      data[offset] = serial.read();

      if (data[offset] == ':')
      {
        data[offset] = 0;
        break;
      }

      offset++;
      if (offset > 16)
        offset = 0;
    }
  }

  getsubstr(data, cleardata, 4, 4);

  Serial.println(cleardata);
  Serial.flush();
}
